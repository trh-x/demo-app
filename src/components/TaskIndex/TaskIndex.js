import React, { useContext } from 'react';
import List, { ListRow } from '../List';
import TaskHeader from './TaskHeader';
import { TaskContext } from '../../state';
import { Item, WideItem, TaskBodyExpanded, TaskLink } from './styled';

const LIST_HEADERS = [
  {
    name: 'Task number',
    isSorted: false
  },
  {
    name: 'Note',
    isSorted: false,
    cellSpan: 2
  },
  {
    name: 'Customer',
    isSorted: false
  },
  {
    name: 'Date',
    isSorted: true
  }
].map((props, index) => <TaskHeader key={index} {...props} />);

function TaskRow({ taskNumber, taskBody, customer, date }) {
  const taskBodyExpanded = (
    <TaskBodyExpanded>
      {taskBody}
      <TaskLink to={`/task-detail/${taskNumber}`}>VIEW TASK</TaskLink>
    </TaskBodyExpanded>
  );

  return (
    <ListRow expandableContent={taskBodyExpanded}>
      <Item>{taskNumber}</Item>
      <WideItem>{taskBody}</WideItem>
      <Item>{customer}</Item>
      <Item>{date}</Item>
    </ListRow>
  );
}

export default function TaskIndex() {
  const { tasks } = useContext(TaskContext);

  return (
    <List title="Tasks" headers={LIST_HEADERS}>
      {tasks.map(task => (
        <TaskRow key={task.taskNumber} {...task} />
      ))}
    </List>
  );
}
