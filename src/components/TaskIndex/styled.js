import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import { COLOR, MOVEMENT } from '../theme';

export const Item = styled.div`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const WideItem = styled(Item)`
  grid-column-start: span 2;
`;

export const TaskBodyExpanded = styled.div`
  display: grid;
  grid-row-gap: 1rem;
  grid-template-rows: auto auto;
`;

export const TaskLink = styled(Link)`
  color: ${COLOR.linkText};
  font-size: 13px;
  font-weight: bold;
  justify-self: end;
  text-decoration: none;
  ${MOVEMENT.hover};
`;
