import styled, { css } from 'styled-components/macro';

import sortedIcon from '../sorted-icon.svg';

export const SortedSpan = styled.span`
  background-image: url(${sortedIcon});
  background-position: left center;
  background-repeat: no-repeat;
  padding-left: 17px;
`;

export const Span = styled.span`
  ${({ cellSpan }) =>
    cellSpan &&
    css`
      grid-column-start: span ${cellSpan};
    `};
`;
