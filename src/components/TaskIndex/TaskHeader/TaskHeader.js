import React from 'react';
import { SortedSpan, Span } from './styled';

export default function TaskHeader({ name, isSorted, cellSpan }) {
  return isSorted ? (
    <SortedSpan>{name}</SortedSpan>
  ) : (
    <Span cellSpan={cellSpan}>{name}</Span>
  );
}
