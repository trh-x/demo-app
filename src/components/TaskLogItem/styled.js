import styled from 'styled-components/macro';

export const Textarea = styled.textarea`
  height: 80px;
  margin-top: 26px;
  width: 100%;
`;

export const Footer = styled.footer`
  display: grid;
  grid-template-columns: repeat(auto-fit, 168px);
  justify-content: end;
  padding-top: 26px;
`;
