import React, { useContext, useState } from 'react';
import { useParams, withRouter } from 'react-router-dom';
import List from '../List';
import Button, { ButtonLink } from '../Button';
import { TaskContext } from '../../state';
import { addLogItem } from '../../state/events';
import { Textarea, Footer } from './styled';

function TaskLogItem({ history }) {
  const { dispatch, currentUser } = useContext(TaskContext);
  const { taskNumber, logType } = useParams();
  const [details, setDetails] = useState();

  const onSubmit = () => {
    dispatch(addLogItem(taskNumber, logType, details, currentUser));
    history.push({ pathname: `/task-detail/${taskNumber}` });
  };

  const footer = (
    <Footer>
      <Button disabled={!details} onClick={onSubmit}>
        OK
      </Button>
      <ButtonLink to={`/task-detail/${taskNumber}`}>CANCEL</ButtonLink>
    </Footer>
  );

  const onChange = ({ target: { value } }) => setDetails(value);

  return (
    <List title={`Log ${logType}`} footer={footer}>
      <Textarea
        value={details}
        onChange={onChange}
        placeholder={`Add ${logType} details here...`}
      />
    </List>
  );
}

export default withRouter(TaskLogItem);
