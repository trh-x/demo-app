import React, { useState } from 'react';
import Styled, {
  ArrowDownIcon,
  ArrowUpIcon,
  ExpandableList,
  Expanded,
  ResponsiveList,
  ResponsiveContent
} from './styled';

export default Styled(ListRow);

function ExpandableRow({ expandableContent, className, children }) {
  const [isExpanded, setIsExpanded] = useState(false);
  const toggle = () => setIsExpanded(!isExpanded);

  const ArrowIcon = isExpanded ? ArrowUpIcon : ArrowDownIcon;

  return (
    <div className={className}>
      <ExpandableList>
        <ResponsiveContent>{children}</ResponsiveContent>
        <ArrowIcon onClick={toggle} />
      </ExpandableList>
      {isExpanded && <Expanded>{expandableContent}</Expanded>}
    </div>
  );
}

function ListRow({ expandableContent, className, children }) {
  return expandableContent ? (
    <ExpandableRow expandableContent={expandableContent} className={className}>
      {children}
    </ExpandableRow>
  ) : (
    <ResponsiveList className={className}>{children}</ResponsiveList>
  );
}
