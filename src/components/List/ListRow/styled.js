import styled from 'styled-components/macro';
import { COLOR, MOVEMENT } from '../../theme';

import arrowDownIcon from './arrow-down-icon.svg';
import arrowUpIcon from './arrow-up-icon.svg';

const rowStyle = `
  display: grid;
  font-size: 12px;
  grid-gap: 1rem;
  grid-template-columns: repeat(auto-fit, minmax(90px, 1fr));
  padding: 10px 26px;
`;

export const ResponsiveList = styled.li`
  ${rowStyle}
`;
export const ResponsiveContent = styled.div`
  ${rowStyle}
`;

const arrowStyle = `
  background-repeat: no-repeat;
  cursor: pointer;
  height: 7px;
  width: 12px;
  ${MOVEMENT.hoverSmall};
`;

export const ArrowDownIcon = styled.div`
  background-image: url(${arrowDownIcon});
  ${arrowStyle}
`;

export const ArrowUpIcon = styled.div`
  background-image: url(${arrowUpIcon});
  ${arrowStyle}
`;

export const ExpandableList = styled.li`
  align-items: center;
  display: grid;
  grid-template-columns: auto 38px;
`;

export const Expanded = styled.div`
  font-size: 12px;
  padding: 6px 26px 15px 9px;
`;

export default ListRow => styled(ListRow)`
  &:nth-child(even) {
    background: ${COLOR.listAltRow};
  }
`;
