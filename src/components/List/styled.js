import styled from 'styled-components/macro';
import { COLOR } from '../theme';

export default List => styled(List)`
  background: ${COLOR.white};
  border-radius: 2px;
  box-shadow: 0 4px 4px 0 rgba(0, 0, 0, 0.26), 0 7px 14px 0 rgba(0, 0, 0, 0.19);
  margin-bottom: 40px;
  padding: 22px;
  &:last-child {
    margin-bottom: 0;
  }
`;

export const Title = styled.h2`
  border-bottom: 1px solid ${COLOR.divider};
  color: ${COLOR.headerText};
  font-size: 18px;
  padding-bottom: 8px;
`;

export const ListHeader = styled.ul`
  color: ${COLOR.headerText};
  font-weight: bold;
  padding-right: 38px;
  padding-top: 26px;
`;
