import React from 'react';
import ListRow from './ListRow';
import Styled, { Title, ListHeader } from './styled';

export default Styled(List);

function ListHeaders({ headers }) {
  return (
    <ListHeader>
      <ListRow>{headers}</ListRow>
    </ListHeader>
  );
}

function List({ title, headers, footer, className, children }) {
  return (
    <section className={className}>
      <header>
        <Title>{title}</Title>
      </header>
      {headers && <ListHeaders headers={headers} />}
      <ul>{children}</ul>
      {footer}
    </section>
  );
}
