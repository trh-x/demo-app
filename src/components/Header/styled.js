import styled from 'styled-components/macro';
import { Link } from 'react-router-dom';
import { COLOR, MOVEMENT } from '../theme';

export const AppHeader = styled.header`
  align-items: center;
  background: ${COLOR.brandBackground};
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.23), 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  display: flex;
  height: 66px;
  justify-content: space-between;
  overflow: hidden;
  padding: 0 60px;
`;

export const LogoLink = styled(Link)`
  ${MOVEMENT.hover};
`;

export const Logo = styled.img`
  animation-delay: 1s;
  animation-duration: 2s;
  animation-name: spin;

  @keyframes spin {
    from {
      transform: rotateY(0deg);
    }
    to {
      transform: rotateY(360deg);
    }
  }
`;

export const LogoutLink = styled.a`
  color: ${COLOR.white};
  cursor: pointer;
  text-decoration: none;
  ${MOVEMENT.hover};
`;
