import React, { useContext } from 'react';
import { withRouter } from 'react-router-dom';
import { AppHeader, LogoLink, Logo, LogoutLink } from './styled';
import { clearCurrentUser } from '../../authentication';
import { TaskContext } from '../../state';

import techAnvilLogo from './tech-anvil-logo.png';

function Logout({ history }) {
  const { currentUser, setCurrentUser } = useContext(TaskContext);

  const onClick = () => {
    clearCurrentUser();
    setCurrentUser(undefined);
    history.push({ pathname: '/login' });
  };

  return <LogoutLink onClick={onClick}>Log out {currentUser}</LogoutLink>;
}

function Header({ history, className }) {
  const { currentUser } = useContext(TaskContext);

  return (
    <AppHeader className={className}>
      <LogoLink to="/">
        <Logo src={techAnvilLogo} />
      </LogoLink>
      {currentUser && <Logout history={history} />}
    </AppHeader>
  );
}

export default withRouter(Header);
