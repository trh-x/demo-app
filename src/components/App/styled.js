import styled, { createGlobalStyle } from 'styled-components/macro';
import { COLOR, FONT } from './../theme';

export const GlobalStyle = createGlobalStyle`
  body {
    background: ${COLOR.defaultBackground};
    color: ${COLOR.defaultText};
    font-family: ${FONT.defaultFamily};
    input, textarea {
      padding: 0.5rem;
    }
  }
`;

export const Main = styled.main`
  margin: 0 auto;
  max-width: 1280px;
  padding: 60px;
`;
