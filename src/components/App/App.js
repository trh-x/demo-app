import React, { useEffect, useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route as PublicRoute
} from 'react-router-dom';
import { Reset } from 'styled-reset';
import { useImmerReducer } from 'use-immer';

import {
  AuthenticatedRoute as Route,
  getCurrentUser
} from '../../authentication';
import Header from '../Header';
import TaskIndex from '../TaskIndex';
import TaskDetail from '../TaskDetail';
import TaskLogItem from '../TaskLogItem';
import Login from '../Login';
import {
  initialTasks,
  persistTasks,
  taskReducer,
  TaskContext
} from '../../state';
import { GlobalStyle, Main } from './styled';

function AppShell({ children }) {
  return (
    <>
      <Reset />
      <GlobalStyle />
      <Header />
      <Main>{children}</Main>
    </>
  );
}

function App() {
  const [tasks, dispatch] = useImmerReducer(taskReducer, initialTasks);
  const [currentUser, setCurrentUser] = useState(getCurrentUser());

  useEffect(() => persistTasks(tasks));

  const state = {
    tasks,
    dispatch,
    currentUser,
    setCurrentUser
  };

  return (
    <TaskContext.Provider value={state}>
      <Router>
        <AppShell>
          <Switch>
            <PublicRoute path="/login">
              <Login />
            </PublicRoute>
            <Route path="/task-detail/:taskNumber/:logType">
              <TaskLogItem />
            </Route>
            <Route path="/task-detail/:taskNumber">
              <TaskDetail />
            </Route>
            <Route path="/task-index">
              <TaskIndex />
            </Route>
            <Route path="/">
              <Redirect to="/task-index" />
            </Route>
          </Switch>
        </AppShell>
      </Router>
    </TaskContext.Provider>
  );
}

export default App;
