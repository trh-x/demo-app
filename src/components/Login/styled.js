import styled from 'styled-components/macro';
import { ListRow } from '../List';

export const Footer = styled.footer`
  margin: 0 auto;
  padding-top: 26px;
  width: 160px;
`;

export const LoginRow = styled(ListRow)`
  align-items: center;
  grid-template-columns: 1fr 160px 1fr;
  &:first-child {
    margin-top: 26px;
  }
  &:nth-child(even) {
    background: none;
  }
`;

export const Label = styled.label`
  justify-self: end;
`;
