import React, { useContext, useState } from 'react';
import { withRouter } from 'react-router-dom';
import List from '../List';
import Button from '../Button';
import { TaskContext } from '../../state';
import { persistCurrentUser } from '../../authentication';
import { Footer, LoginRow, Label } from './styled';

function Login({ history }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const { setCurrentUser } = useContext(TaskContext);

  const onSubmit = () => {
    persistCurrentUser(username);
    setCurrentUser(username);
    history.push({ pathname: '/task-index' });
  };

  const footer = (
    <Footer>
      <Button onClick={onSubmit}>OK</Button>
    </Footer>
  );

  const onChange = setter => ({ target: { value } }) => setter(value);

  const onKeyPress = ({ key }) => {
    if (key === 'Enter') onSubmit();
  };

  return (
    <List title="Login" footer={footer}>
      <LoginRow>
        <Label>Username</Label>
        <input
          value={username}
          onChange={onChange(setUsername)}
          onKeyPress={onKeyPress}
          placeholder="Enter username..."
        />
      </LoginRow>
      <LoginRow>
        <Label>Password</Label>
        <input
          value={password}
          onChange={onChange(setPassword)}
          onKeyPress={onKeyPress}
          type="password"
        />
      </LoginRow>
    </List>
  );
}

export default withRouter(Login);
