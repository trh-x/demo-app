import styled, { css } from 'styled-components/macro';
import { ListRow } from '../../List';
import ICON from '../../icons';

export const Icon = styled.div`
  background-position: left center;
  background-repeat: no-repeat;
  height: 40px;
  width: 22px;
  ${({ type }) =>
    type &&
    css`
      background-image: url(${({ type }) => ICON[type]});
      padding-left: 22px;
    `}
`;

export const Details = styled.div`
  flex-basis: 300px;
  flex-grow: 1;
  flex-shrink: 1;
  margin-right: 20px;
`;

export const User = styled.div`
  margin-right: 10px;
`;

export const Footer = styled.footer`
  display: grid;
  grid-template-columns: repeat(auto-fit, 190px);
  justify-content: end;
  padding-top: 26px;
`;

export const LogRow = styled(ListRow)`
  align-items: center;
  display: flex;
  flex-wrap: wrap;
  font-size: 13px;
  justify-content: space-between;
  padding: 14px 9px;
`;
