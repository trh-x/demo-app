import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import List from '../../List';
import { ButtonLink } from '../../Button';
import { TaskContext } from '../../../state';
import { Icon, Details, User, Footer, LogRow } from './styled';

function formatDate(date) {
  return new Date(date)
    .toLocaleDateString('en-GB', {
      dateStyle: 'short',
      timeStyle: 'short'
    })
    .replace(',', '');
}

export default function LogList({ className }) {
  const { tasks } = useContext(TaskContext);
  const { taskNumber } = useParams();

  // TODO: DRY
  const number = parseInt(taskNumber, 10);
  const task = tasks.find(t => t.taskNumber === number);

  const footer = (
    <Footer>
      <ButtonLink icon="call" to={`/task-detail/${taskNumber}/call`}>
        ADD CALL
      </ButtonLink>
      <ButtonLink icon="note" to={`/task-detail/${taskNumber}/note`}>
        ADD NOTE
      </ButtonLink>
    </Footer>
  );

  return (
    <List title="Log" footer={footer} className={className}>
      {task.log.map(({ logType, details, date, user }, index) => (
        <LogRow key={index}>
          <Icon type={logType} />
          <Details>{details}</Details>
          <User>{user}</User>
          <div>{formatDate(date)}</div>
        </LogRow>
      ))}
    </List>
  );
}
