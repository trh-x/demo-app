import styled from 'styled-components/macro';

export const TaskInformation = styled.section`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 1rem;
  padding: 36px 0 10px 0;
`;

export const Customer = styled.div`
  text-transform: uppercase;
  font-weight: bold;
  font-size: 18px;
`;

export const Datestamp = styled.div`
  justify-self: end;
  font-size: 14px;
`;

export const TaskBody = styled.div`
  grid-column: span 2;
  font-size: 12px;
  margin-top: 10px;
`;
