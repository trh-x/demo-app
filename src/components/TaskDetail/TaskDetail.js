import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import List from '../List';
import LogList from './LogList';
import { TaskContext } from '../../state';
import { TaskInformation, Customer, Datestamp, TaskBody } from './styled';

export default function TaskDetail() {
  const { tasks } = useContext(TaskContext);
  const { taskNumber } = useParams();

  const number = parseInt(taskNumber, 10);
  const task = tasks.find(t => t.taskNumber === number);

  const { taskBody, customer, date } = task;

  return (
    <>
      <List title="Task information">
        <TaskInformation>
          <Customer>{customer}</Customer>
          <Datestamp>{date.split(' ')[0]}</Datestamp>
          <TaskBody>{taskBody}</TaskBody>
        </TaskInformation>
      </List>
      <LogList />
    </>
  );
}
