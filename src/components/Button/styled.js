import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components/macro';
import { COLOR, FONT, MOVEMENT } from '../theme';
import ICON from '../icons';

const buttonStyle = `
  background: ${COLOR.buttonBackground};
  background-position: 22px center;
  background-repeat: no-repeat;
  border: none;
  border-radius: 2px;
  box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.12),  0 2px 2px 0 rgba(0, 0, 0, 0.24);
  color: ${COLOR.defaultText};
  cursor: pointer;
  font-family: ${FONT.defaultFamily};
  font-size: 14px;
  height: 36px;
  justify-self: end;
  line-height: 36px;
  text-align: center;
  text-decoration: none;
  transition: background 0.5s, box-shadow 0.5s, color 0.5s;
  width: 127px;

  ${MOVEMENT.hover};

  &[disabled] {
    background: ${COLOR.buttonBackgroundDisabled};
    box-shadow: 0 0 2px 0 rgba(0, 0, 0, 0.1),  0 2px 2px 0 rgba(0, 0, 0, 0.2);
    color: ${COLOR.buttonTextDisabled};
  }
`;

export const ButtonLink = styled(Link)`
  ${buttonStyle}
  ${({ icon }) =>
    icon &&
    css`
      background-image: url(${({ icon }) => ICON[icon]});
      padding-left: 22px;
    `}
`;

export default styled.button`
  ${buttonStyle}
`;
