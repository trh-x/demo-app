import React, { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { TaskContext } from '../state';

export default function AuthenticatedRoute({ children, ...rest }) {
  const { currentUser } = useContext(TaskContext);

  return (
    <Route
      {...rest}
      render={({ location }) =>
        currentUser ? (
          children
        ) : (
          <Redirect to={{ pathname: '/login', state: { from: location } }} />
        )
      }
    />
  );
}
