import AuthenticatedRoute from './AuthenticatedRoute';
export { AuthenticatedRoute };

export function getCurrentUser() {
  return document.cookie.split(/=/)[1];
}

export function persistCurrentUser(userName) {
  document.cookie = 'currentUser=' + userName;
}

export function clearCurrentUser() {
  document.cookie = 'currentUser=';
}
