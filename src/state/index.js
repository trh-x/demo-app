import React from 'react';

import { initialTasks, taskReducer } from './taskReducer';
import { persistTasks } from './persistence';

export { initialTasks, taskReducer, persistTasks };

export const TaskContext = React.createContext();
