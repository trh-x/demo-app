export function persistTasks(tasks) {
  localStorage.setItem('tasks', JSON.stringify(tasks));
}

export function restoreTasks() {
  const tasks = localStorage.getItem('tasks');
  return tasks && JSON.parse(tasks);
}
