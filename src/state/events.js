export const addLogItem = (taskNumber, logType, details, user) => ({
  type: 'LOG_ADD',
  taskNumber: parseInt(taskNumber, 10),
  logType,
  details,
  user,
  date: new Date().getTime()
});
