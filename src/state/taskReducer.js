import { restoreTasks } from './persistence';

import tasks from './tasks.json';

export const initialTasks =
  restoreTasks() || tasks.map(task => ({ ...task, log: [] }));

export function taskReducer(tasks, action) {
  switch (action.type) {
    case 'LOG_ADD':
      const { taskNumber, logType, details, user, date } = action;
      const task = tasks.find(t => t.taskNumber === taskNumber);
      return void task.log.push({ logType, details, user, date });
    default:
      break;
  }
}
