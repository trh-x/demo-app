# Tech Anvil - Demo UI

An example of a simple UI using React 16, Styled Components and Immer (use-immer).

Bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Coded in a hurry, with plenty of rough edges!

## Run

### `npm start`

Runs the app in the development mode - [http://localhost:3000](http://localhost:3000)

## Test

### `npm run test`

Note: test suite is still TODO!

## Build

### `npm run build`

Bundles the app to `./build`.
